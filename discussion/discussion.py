# [SECTION] Lists
names = ["John", "Paul", "George", "Ringo"] #List of strings
programs = ["developer career", "pi-shape", 'short courses'] 
durations = [260, 180, 20] #List of numbers
truth_values = [True, False, True, True, False] #List of boolean values

# For getting the length of the list
print(len(programs))

# For getting an item from the list
print(programs[0])

# For getting items based on an index range
# The first index must be lower than the second one
print(programs[0:2])

# You can update the value of a specific item in the list by calling the item by the index and assigning a new value to it
print(programs[2])

programs[2] = 'Short Courses'

print(programs[2])


# [SECTION] List Manipulation
# Adds a new item onto the list
durations.append(367)
print(durations)

# Deleting an item from the list
del durations[-1]
print(durations)

# For Sorting a list. The sort() method sorts the list by ascending order by default.
names.sort()
print(names)

# For clearing/emptying the list
test_list = [1, 3, 5, 7, 9]
test_list.clear()
print(test_list)

# Nested Dictionaries
person3 = {
	"name": "Monika",
	"age": 21,
	"occupation": "Lyricist",
	"isEnrolled": True,
	"subjects": ["Python", "SQL", "Ruby"]
}

classRoom = {
	"student1": {
		"name": "Elon",
		"age": 70,
		"occupation": "student",
		"isEnrolled": True,
		"subjects": ["Python", "SQL", "Django"]
	},
	"student2": person3
}

def my_greeting():
	print("Hello user!")

my_greeting()

def greet_user(username):
	print(f"Hello, {username}")

greet_user("Elon")

# Lambda Functions
greeting = lambda person : f"Hello {person}!"
print(greeting("Elsie"))
print(greeting("Tony"))

#[SECTION] Classes

class Car():
 def __init__(self,brand,model,year_of_make):
  self.brand = brand
  self.model = model
  self.year_of_make = year_of_make

  #Other properties
  self.fuel = "Gasoline"
  self.fuel_level = 0
 
 def fill_fuel(self):
  print(f"Current fuel level: {self.fuel_level}")
  print('...filling up the fuel tank')
  self.fuel_level = 100
  print(f"New fuel level: {self.fuel_level}")

 def distance(self):
  self.distanceTravelled = 59
  print(f"Distance: {self.distanceTravelled}")
  self.fuel_level -= self.distanceTravelled

  print(f"Current fuel level: {self.fuel_level}")

 
new_car = Car("Maserati", "Explorer", "2005")

print(f"My car is a {new_car.brand} {new_car.model}")

new_car.fill_fuel()
new_car.distance()